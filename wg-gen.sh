#!/bin/bash

help() {
    printf "Usage: wg-gen [-i | -iname]
    [--client-config]\t\tProduce client config (may be combined with --server-config)
    [--server-config]\t\tProduce server config (may be combined with --client-config)
    [-a | --address]\t\tVPN subnetwork (in CIDR)
    [-e | --endpoint]\t\tServer public IP
    [-p | --port]\t\tWireguard port
    [--allowed-ips]\t\tClient ip range (in CIDR)
    [--defaults]\t\tUse default port and ipt parameters
    [-h | --help]\t\tPrint this help message\n"
    exit 0
}

# Some bash boilerplate you can skip this part

SHORT=i:,a:,e:,p:,h
LONG=iname:,address:,endpoint:,port:,help,allowed-ips:,defaults
LONG=${LONG},client-config,server-config
OPTS=$(getopt --alternative \
    --name wg-gen \
    --options ${SHORT} \
    --longoptions ${LONG} -- "$@")

# If there is no given parameters show help
if [ $# -eq 1 ]; then
    help
fi

client_config=false
server_config=false
DEFAULT=false

while [[ $# -gt 0 ]]; do
    case "$1" in
        -i|--iname)
            interface_name="$2"
            shift 2
            ;;
        -a|--address)
            address="$2"
            shift 2
            ;;
        -e|--endpoint)
            endpoint="$2"
            shift 2
            ;;
        -p|--port)
            port="$2"
            shift 2
            ;;
        -h|--help)
            help
            ;;
        --allowed-ips)
            allowed_ips="$2"
            shift 2
            ;;
        --defaults)
            DEFAULT=true
            shift
            ;;
        --client-config)
            client_config=true
            shift
            ;;
        --server-config)
            server_config=true
            shift
            ;;
        *)
            echo "Unexpected option: $1"
            exit 2
    esac
done

if $DEFAULT; then
    port=51820
    address="10.10.10.1/24"
    allowed_ips="10.10.10.2/32"
fi

################################################################################
# Application
################################################################################

gen_key_pair() {
    local private_key
    private_key=$(wg genkey)
    local public_key
    public_key=$(echo "${private_key}" | wg pubkey)
    echo "${private_key} ${public_key}"
}

if $server_config; then
    # server private and public keys
    serv_pair=$(gen_key_pair)
    serv_private=$(echo "${serv_pair}" | awk '{print $1}')
    serv_public=$(echo "${serv_pair}" | awk '{print $2}')

    if ! $client_config; then
        client_public="# fill public client key"
    fi
fi

if $client_config; then
    # client private and public keys
    client_pair=$(gen_key_pair)
    client_private=$(echo "${client_pair}" | awk '{print $1}')
    client_public=$(echo "${client_pair}" | awk '{print $2}')

    if ! $server_config; then
        serv_public="# fill public sever key"
    fi
fi

$server_config && echo "
[Interface]
Address = ${address}
ListenPort = ${port}
# PublicKey = ${serv_public}
PrivateKey = ${serv_private}

[Peer]
PublicKey = ${client_public}
AllowedIPs = ${allowed_ips}
"

$client_config && echo "
[Interface]
Address = ${allowed_ips}
# PublicKey = ${client_public}
PrivateKey = ${client_private}

[Peer]
PublicKey = ${serv_public}
AllowedIPs = 0.0.0.0/0
Endpoint = ${endpoint}:${port}
"
