# wg-gen

Small bash script to generate config template for wireguard. Can
generate either server config and client config.

At this moment this script is needed just to improve my QoL, I'll be
happy if this software will help someone from the internet.

## Motivation

I have needed a convenient script to generate templates for Wireuard.

## Usage

To get a default setup for a server and a client:
```
wg-gen --defaults --client-config --server-config --endpoint x.x.x.x
```
You will get the server config which will listen on 51820 port and the
client with one IP address 10.10.10.2/32. Example of output:
```conf
[Interface]
Address = 10.10.10.1/24
ListenPort = 51820
PrivateKey = wI3NS/hZFBFLdwwqE5w1LTYh8h7Se0DswBA98QJuW30=

[Peer]
PublicKey = uEHjjkToH3MIojwQHUhhgDfbPwLt4a/31NYv+YUDUl4=
AllowedIPs = 10.10.10.2/32


[Interface]
Address = 10.10.10.2/32
PrivateKey = 6LvvmrMf1oCJGOxCMlqo+lcuruW5ILY26oWFjWJiiXs=

[Peer]
PublicKey = yI0lQhA3tKFsayjWDo57epj3fOee0nKuXGz1NsBbk3g=
AllowedIPs = 0.0.0.0/0
Endpoint = x.x.x.x:51820
```

## Contributing

Feel free to open issues for feature requests and bugs.
